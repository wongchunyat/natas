# Natas 17

We can't even know if there are any matching rows now,
however, we can still do blind sql injection, and use the `SLEEP`
function to manipulate the response time.

## Query

```sql
SELECT * FROM users WHERE username="natas18" AND BINARY password LIKE "password%" AND SLEEP(20); #"
```

Here, the query would wait for 20 seconds before returning if the previous conditions are matched.
I used 20 seconds because the requests are already slow, so normal requests could also take
~10 seconds.

## Script

Run [bruteforce.ts](./bruteforce.ts)

If you found an array item having more than one characters, you should increase the time
and re-run.

Due to that we have to wait 20 seconds for each item, the script takes > 10 minutes to complete the brute force (my run was 13.5 mins).
