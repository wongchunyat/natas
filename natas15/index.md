# Natas 15

## Analyze

Seems to be an sql injection exploit.

However, we can only see if there are any rows that match the query.

Therefore, we have to check if the password matches a pattern using
`SELECT * FROM users WHERE username="natas16" BINARY password LIKE "${char}%"`

## Exploit

Using a simple script [bruteforce.sh](./bruteforce.sh), we can brute force the password one character at a time in 10 minutes.

**NOTE**: The script must be executed with zsh, not bash.
