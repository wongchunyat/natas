# Natas 19

Source code is not provided anymore, but the page says the source code
is similar to [natas 18](../natas18/), but that the php session ids are
no longer sequential.

By trying to log in, we got the session id 312d61646d696e, which is unlike
the integer value in natas18.

However, this doesn't seem like a normal php session id at all (normally it is a long
integer). So, it's probably encoded something.

Not a php developer myself, I asked chatgpt to analyze it, and it said it is probably
encoded by a `bin2hex` function, also the decoded version of my php session id is actually
`362-admin`.

Here, we can assume that the change is just suffixing with `-admin` and encoding.

Therefore, we can still try 640 times with encoded values.

## Script

By running [steal_session.ts](./steal_session.ts), we found that this time the session
id (decoded) is `281-admin`.
