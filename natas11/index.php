<?php
function xor_decrypt($in, $out) {
    $text = $out;
    $outText = '';

    // Iterate through each character
    for($i=0;$i<strlen($text);$i++) {
    $outText .= $text[$i] ^ $in[$i % strlen($in)];
    }

    return $outText;
}
echo xor_decrypt(json_encode(array( "showpassword"=>"no", "bgcolor"=>"#ffffff")), base64_decode('MGw7JCQ5OC04PT8jOSpqdmkgJ25nbCorKCEkIzlscm5oKC4qLSgubjY='));
echo nl2br("\n");

function xor_encrypt($in, $key) {
    $text = $in;
    $outText = '';

    // Iterate through each character
    for($i=0;$i<strlen($text);$i++) {
    $outText .= $text[$i] ^ $key[$i % strlen($key)];
    }

    return $outText;
}
echo base64_encode(xor_encrypt(json_encode(array("showpassword"=>"yes", "bgcolor"=>"#ffffff")), 'KNHL'));
?>
